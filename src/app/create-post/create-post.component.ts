import { Component } from '@angular/core';
import { PostService } from '../post.service';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.css']
})


  

export class CreatePostComponent {
  caption: string = '';
  selectedImage: File | null = null;
  postResponse$!: Observable<any>; // Observable to hold the post response

  constructor(private postService: PostService) {}

  onFileChange(event: any) {
    if (event.target.files.length > 0) {
      this.selectedImage = event.target.files[0];
    }
  }

  submitPost() {
    if (this.selectedImage && this.caption) {
      const formData = new FormData();
      formData.append('image', this.selectedImage);
      formData.append('caption', this.caption);

      this.postResponse$ = this.postService.createPost(formData);
    }
  }
}
